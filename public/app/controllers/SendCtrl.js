angular.module('myApp.controllers')
.controller('SendCtrl', function ($scope, $http) {
    $scope.hello = [];
    $scope.mama = "";
    $scope.sendPost = function() {
        var data = $.param({
            json: JSON.stringify({
                name: $scope.name,
                email: $scope.email,
                message: $scope.message
            })
        });
        $http.post("/contact", data).success(function(data, status) {
            $scope.hello = data;
        })
    }                   
})