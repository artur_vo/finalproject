angular.module('myApp.controllers')
.controller('TournamentCtrl', ['$scope', 'posts', 'post', 'auth',
function($scope, posts, post, auth) {
	$scope.post = post;
	$scope.isLoggedIn = auth.isLoggedIn;
  $scope.scoreTable = [];
  $scope.increment = 0;
	

  $scope.addComment = function() {
		if ($scope.body === '') {
			return;
		}
		posts.addComment(post._id, {
			body : $scope.body,
			author : 'user'
		})
		.success(function(comment) {
			$scope.post.comments.push(comment);
		});

		$scope.body = '';
	};

	$scope.addUser = function() {

		if (post.num_of_teams === $scope.post.joinusers.length) {
			$scope.fullpeople = true;
      return;
		}
		if ($scope.teamname === '') {
			return;
		}
		posts.addUser(post._id, {
			teamname : $scope.teamname,
			author : 'user'
		})
		.success(function(joinuser) {
			$scope.post.joinusers.push(joinuser);
		});
		$scope.teamname = '';
	};

	$scope.upvote = function(comment) {
    console.log(comment + " comment");
		posts.upvoteComment(post, comment);
	};

	$scope.downvote = function(comment) {
		posts.downvoteComment(post, comment);
	};

  $scope.returnDay = function(day){
    return new Date().toISOString().slice(0,10);
  }

  // function shuffle(a) 
  //   {
  //     var j, x, i;
  //     for (i = a.length; i; i -= 1) {
  //         j = Math.floor(Math.random() * i);
  //         x = a[i - 1];
  //         a[i - 1] = a[j];
  //         a[j] = x;
  //     }
  // }
  

  $scope.participants = [];
  $scope.knoRounds  = [];
  // Metrics in the nav bar
  $scope.numberPlayers = 0;
  $scope.numberRounds = 0;
$scope.isOwner = false;

  $scope.$watch(function() {
        
        $scope.currentUser = auth.currentUser;
        if($scope.currentUser() == post.author){
            $scope.isOwner = true;
        }else{
            $scope.isOwner = false;
        }
    });

  ////////////////////////////////////////////////
  // Nav bar metrics
  ////////////////////////////////////////////////
  $scope.matchesInRound = function(numPlayers) {
    var matches = Math.round(numPlayers / 2);
    return matches;
  };

  $scope.numberOfRounds = function(numPlayers){
    var rounds = 0;
    var roundCounter = function(players) {
      var matches = $scope.matchesInRound(players);
    
      if (matches === 1) {
        rounds++;
        return;
      } else {
        rounds++;
        roundCounter(matches);
      }
    };
    if ( numPlayers ) {
      roundCounter(numPlayers);
    }
    return rounds;
  };

  $scope.getParticipants = function() {

        var counter = 1;
        for ( var i = 0; i < $scope.post.joinusers.length; i++ ) {
            $scope.participants.push($scope.post.joinusers[i].teamname);
            counter++;
        }
        console.log( $scope.participants);
        $scope.numberPlayers = counter - 1;
        $scope.numberRounds = $scope.numberOfRounds($scope.numberPlayers);
        for ( var j = 0; j < $scope.numberRounds +1 ; j++) {
          $scope.knoRounds.push((j + 1));
        }
        
  };
 
  //need watch for this 
    $scope.$watch(function( value ) {
      if($scope.fullpeople == false){
        $scope.getParticipants(); 
      }
  },true);

         


 //console.log($scope.knoRounds);
  $scope.getMatches = function(matches) {

        $scope.knoMatches = [];
        var count = 0;
        for ( var i = 0; i < $scope.matchesInRound($scope.post.joinusers.length); i++ ) {
            $scope.knoMatches.push({
              playerA: $scope.participants[count],  
              playerB: $scope.participants[count+1], 
              round: 1 , 
              winner: 1
            });
          count+=2;
        }
    };



 $scope.winRound = function(match, winner) {

    // If the match is odd, then it should go in PlayerOne
    var matchIndex = $scope.knoMatches.indexOf(match);
    var roundCount = $scope.numberRounds;
        // If it's the final match
    if (match.round === $scope.numberRounds) {
        console.log(match.winner);          
    }

  };

if(post.tournament_type == "Knockout"){
  $scope.getMatches();
}
 	




$scope.index1 = 0;
$scope.index2 = 0;

$scope.schedule = function() {
    if (!round) {
        var array = $scope.participants;
        var teams = array.length,
            halfTour = (teams - 1),
            matchesPerRound = teams / 2,
            totalRounds = halfTour * post.num_of_game_against,
            round,
            swap,
            match,
            currentRoundText,
            home,
            away;
            $scope.rounds = [];           
            $scope.matches = [];
            $scope.allGames =  matchesPerRound * totalRounds;
        for (round = 0; round < totalRounds; round++) {
            currentRoundText = [(round + 1)];

            for (match = 0; match < matchesPerRound; match++) {
                home = (round + match) % (teams - 1);
                away = (teams - 1 - match + round) % (teams - 1);
                if (match === 0) {
                    away = teams - 1;
                }
                if (round >= halfTour) { 
                    swap = home;
                    home = away;
                    away = swap;
                }
                currentRoundText += ('[' + array[home]+ ' ' + array[away] + ']');
                $scope.matches.push({
                  home:array[home], 
                  away:array[away],
                  homescore: $scope.homescore,
                  awayscore: $scope.awaycore,
                  id: $scope.index1,
                  round: round
                });
                $scope.index1 += 2;

            }  

            // console.log($scope.matches);
            //$scope.rounds.push($scope.matches[0]);
        }
        return $scope.rounds;
    return schedule(round - 1); 
  }
}

$scope.schedule();

$scope.arr = [];
$scope.count = 2;
$scope.i = 0;


    $scope.add = function(i, index) { 
              var index1,index2;              
                index1 = i; 
                index2 = i+1;
              if(i != 0){
                i = i/2;
              }
              console.log(i);
              console.log(index1);
              console.log(index2);
              var home = $scope.matches[i].home;
              var homescore = $scope.matches[i].homescore; 
              var away = $scope.matches[i].away; 
              var awayscore = $scope.matches[i].awayscore;
              var round = $scope.matches[i].round;

            if(homescore != undefined && awayscore !=undefined){
                $scope.checkscore(home,away,homescore,awayscore,round,index1,index2,index);
            }
              
    };
      

      $scope.checkscore = function(homeTeam,awayTeam,homeScore,awayScore,round,index1,index2,index){
      var team = homeTeam;
      var team2 = awayTeam;
      var score = homeScore;
      var score2 = awayScore;
      var round2 = round;
      // console.log(team + "  " + team2);
      // console.log($scope.scoreTable);
      if(index != 0){
        index += 2;
      }
      var wins,draws,losses,difference,points,wins2, losses2,difference2,points2;
      var check = true;
      
      for(var i = 0; i < $scope.scoreTable.length; i++){
          if($scope.scoreTable[i].team == team && round == $scope.scoreTable[i].round ){
              $scope.scoreTable[i].score = score;
              $scope.scoreTable[i].against = score2;
              check = false;
            if(score == score2){
                $scope.scoreTable[i].losses = 0;
                $scope.scoreTable[i].wins = 0;
                $scope.scoreTable[i].draws = 1;
                $scope.scoreTable[i].points = 1;
                
            } 
            else if(score > score2){
                $scope.scoreTable[i].wins = 1;
                $scope.scoreTable[i].points = 3;
                $scope.scoreTable[i].losses = 0;
                $scope.scoreTable[i].draws = 0;
            } else{
                $scope.scoreTable[i].losses = 1;
                $scope.scoreTable[i].points = 0;
                $scope.scoreTable[i].wins = 0;
                $scope.scoreTable[i].draws = 0;
            }
          }
          if($scope.scoreTable[i].team == team2 && round == $scope.scoreTable[i].round ){
              $scope.scoreTable[i].against = score;
              $scope.scoreTable[i].score = score2;
              check = false;
              if(score == score2){
                $scope.scoreTable[i].losses = 0;
                $scope.scoreTable[i].wins = 0;
                $scope.scoreTable[i].draws = 1;
                $scope.scoreTable[i].points = 1;
              } 
              else if(score < score2){
                $scope.scoreTable[i].wins = 1;
                $scope.scoreTable[i].points = 3;
                $scope.scoreTable[i].losses = 0;
                $scope.scoreTable[i].draws = 0;
              } 
              else{
                $scope.scoreTable[i].losses = 1;
                $scope.scoreTable[i].points = 0;
                $scope.scoreTable[i].wins = 0;
                $scope.scoreTable[i].draws = 0;
              }
          }

      }

      if(check == true){
        if(score == score2){
            draws = 1;
            points = 1;
            points2 = 1;
        } 
        else if(score > score2){
           wins = 1;
           losses2 = 1;
           points = 3;
           points2 = 0;
        } 
        else{
           wins2 = 1;
           losses = 1;
           points2 = 3;
           points = 0; 
        } 
        $scope.scoreTable.push({
          id: index1,
          team: team,
          score: score,
          round: round2,
          wins: wins,
          draws: draws,
          losses: losses,
          against: score2,
          difference: difference,
          points: points
        });

        $scope.tableScore($scope.scoreTable.length-1);
        $scope.scoreTable.push({
          id: index2,
          team: team2,
          score: score2,
          round: round2,
          wins: wins2,
          draws: draws,
          losses: losses2,
          against: score,
          difference: difference2,
          points: points2          
        });
        $scope.tableScore($scope.scoreTable.length-1);

      }

    }



  $scope.tableScore = function(i) {
      posts.tableScore(post._id, {
          id: $scope.scoreTable[i].id,
          team:  $scope.scoreTable[i].team,
          score:  $scope.scoreTable[i].score,
          round:  $scope.scoreTable[i].round,
          wins:  $scope.scoreTable[i].wins,
          draws:  $scope.scoreTable[i].draws,
          losses:  $scope.scoreTable[i].losses,
          against:  $scope.scoreTable[i].against,
          difference:  $scope.scoreTable[i].difference,
          points:  $scope.scoreTable[i].points
      })
      .success(function(match) { 
          $scope.post.allmatches.push(match);
      }); 
  }; 
  $scope.teams = [];
   //find value in object

  $scope.$watch("post.allmatches", function( value ) {
      $scope.updateTable();
  },true);

  function has(obj, value) {
    for(var i in obj) {
      if(obj[i].team == value) {
        return i;
      }
    }
    return null;
  }
  

  $scope.updateTable = function() {
      for (var i = $scope.increment; i < $scope.post.allmatches.length; i++) {
        if(has($scope.teams,$scope.post.allmatches[i].team) != null){

            var j = has($scope.teams,$scope.post.allmatches[i].team);
            // $scope.scores = $scope.team[j].score + scope.post.allmatches[i].score;

            $scope.teams[j].score = $scope.teams[j].score + $scope.post.allmatches[i].score;
            $scope.teams[j].wins = $scope.teams[j].wins + $scope.post.allmatches[i].wins;
            $scope.teams[j].draws = $scope.teams[j].draws + $scope.post.allmatches[i].draws;
            $scope.teams[j].losses = $scope.teams[j].losses + $scope.post.allmatches[i].losses;
            $scope.teams[j].against = $scope.teams[j].against + $scope.post.allmatches[i].against;
            $scope.teams[j].difference = $scope.teams[j].score - $scope.post.allmatches[i].against;
            $scope.teams[j].points = $scope.teams[j].points + $scope.post.allmatches[i].points;

        }
        else{
            $scope.teams.push({
              team: $scope.post.allmatches[i].team,
              score: $scope.post.allmatches[i].score,
              wins: $scope.post.allmatches[i].wins,
              draws: $scope.post.allmatches[i].draws,
              losses: $scope.post.allmatches[i].losses,
              against: $scope.post.allmatches[i].against,
              difference: $scope.post.allmatches[i].difference,
              points: $scope.post.allmatches[i].points          
            });
        }
        $scope.increment += 1;
      };
  };
  

  

	$scope.getNumberOfTeams = function(num) {

    	return new Array(num);   
	}
}]);
