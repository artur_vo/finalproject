describe('Post Controller', function () {
 
  var $controller , posts , PostCtrl;
  
  beforeEach(module('myApp.controllers'));
  beforeEach(module('myApp.factory'));

  beforeEach(inject(function (_$controller_, _posts_ ) {
    	posts = _posts_;
    	$controller = _$controller_;
  }));

  	it('should check the value', function () {
  	   var $scope = {};
	   var controller = $controller('PostCtrl', { $scope: $scope });
	   $scope.numofteams = 2;
	   $scope.incNumOfTeams();
	   expect(scope.numofteams).toBe(2);
	});

});