var app = angular.module('bodyApp', ['ui.router', 'myApp.controllers', 'myApp.factory', 'myApp' , 'ngAnimate']);

app.run(function ($state,$rootScope) {
    $rootScope.$state = $state;
})

app.config(['$stateProvider', '$urlRouterProvider',
function($stateProvider, $urlRouterProvider) {

	$stateProvider.state('home', {
		url : '/home',
		templateUrl : 'app/views/home.html',
		controller : 'PostCtrl',
		resolve : {
			postPromise : ['posts',
			function(posts) {
				return posts.getAll();
			}]

		}

	}).state('create', {
		url : '/create',
		templateUrl : 'app/views/create.html',
		controller : 'PostCtrl',
		resolve : {
			postPromise : ['$stateParams','posts',
			function($stateParams,posts) {
				return posts.getAll();
			}]

		}
	}).state('posts', {
		url : '/posts/:id',
		templateUrl : 'app/views/posts.html',
		controller : 'TournamentCtrl',
		resolve : {
			post : ['$stateParams', 'posts',
			function($stateParams, posts) {
				return posts.get($stateParams.id);
			}]

		}
	}).state('login', {
		url : '/login',
		templateUrl : 'app/views/login.html',
		controller : 'AuthCtrl',
		onEnter : ['$state', 'auth',
		function($state, auth) {
			if (auth.isLoggedIn()) {
				$state.go('home');
			}
		}]

	})
	.state('register', {
		url : '/register',
		templateUrl : 'app/views/register.html',
		controller : 'AuthCtrl',
		onEnter : ['$state', 'auth',
		function($state, auth) {
			if (auth.isLoggedIn()) {
				$state.go('home');
			}
		}]

	})
	.state('tournaments', {
		url : '/tournaments',
		templateUrl : 'app/views/tournaments.html',
		controller : 'PostCtrl',
		resolve : {
			postPromise : ['posts',
			function(posts) {
				return posts.getAll();
			}]

		}
	})
	.state('dashboard', {
		url : '/dashboard',
		templateUrl : 'app/views/dashboard.html',
		controller : '',

	})
	.state('contact', {
		url : '/contact',
		controller: 'SendCtrl',
		templateUrl : 'app/views/contact.html'
	});
	$urlRouterProvider.otherwise('home');
}]);

