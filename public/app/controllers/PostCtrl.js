angular.module('myApp.controllers')
.controller('PostCtrl', ['$scope', 'posts', 'auth','$mdpDatePicker', '$mdpTimePicker' , 
function($scope, posts, auth, $mdpDatePicker, $mdpTimePicker){
	$scope.posts = posts.posts;
	$scope.isLoggedIn = auth.isLoggedIn;
	$scope.title = '';
	$scope.imageUrl = '';
	$scope.sport_type = '';
	$scope.tournament_type = '';
	$scope.numofteamsinknoc = '';
	$scope.description = '';
	$scope.isCollapsed = true;

	$scope.types = [
      '',
	  'Soccer',
	  'Basketball',
	  'Rugby',
	  'GAA'
    ];
    $scope.tournaments = [
    	'',
    	'League',
    	'Knockout',
    	'League and knockout'
    ];
    $scope.teams_knockout = [
      '',
	  '32 teams (Round of 32, Round of 16, Quarter final, semi-final and final)',
	  '16 teams (Round of 16, Quarter final, semi-final and final)',
	  '8 teams  (Quarter final, semi-final and final)',
	  '4 teams  (Semi-final and final)',
	  '2 teams  (Final)'
    ];

	$scope.numofteams = 2;
	$scope.incNumOfTeams = function() {
		if($scope.numofteams < 32){
			$scope.numofteams += 1;
		}
	};	  
	$scope.decNumOfTeams = function() {
		if(!($scope.numofteams <= 2)){
			$scope.numofteams -= 1;
		}
	};

	$scope.numofplayers = 1;
	$scope.incNumOfPlayers = function() {
		if($scope.numofplayers < 28){
	   		$scope.numofplayers += 1;
		}
	};	  
	$scope.decNumOfPlayers = function() {
	  	if(!($scope.numofplayers <= 1)){
	    	$scope.numofplayers -= 1;
	    }
	};

	$scope.numofgameagainst = 1;
	$scope.incNumOfGamesAg = function() {
	   if($scope.numofgameagainst < 2)
	   	  $scope.numofgameagainst += 1;
	};	  
	$scope.decNumOfGamesAg = function() {
	  	if(!($scope.numofgameagainst <= 1)){
	    	$scope.numofgameagainst -= 1;
	    }
	};
	$scope.knockagainst = 1;
	$scope.incKnockAg = function() {
	   if($scope.knockagainst < 2)
	   	$scope.knockagainst += 1;
	};	  
	$scope.decKnockAg = function() {
	  	if(!($scope.knockagainst <= 1)){
	    	$scope.knockagainst -= 1;
	    }
	};


	$scope.knockagainstfinal = 1;
	$scope.incFinalGamesAg = function() {
		if($scope.knockagainstfinal < 2)
	   		$scope.knockagainstfinal += 1;
	};	  
	$scope.decFinalGamesAg = function() {
	  	if(!($scope.knockagainstfinal <= 1)){
	    	$scope.knockagainstfinal -= 1;
	    }
	};

	

	  $scope.$watch(function() {
	  	  if($scope.tournament_type === 'League'){
   			$scope.league = true;
   			$scope.knockout = false;
			$scope.addPost = function() {
				if ($scope.title === '') {
					return;
				}
				posts.create({
					title : $scope.title,
					sport_type : $scope.sport_type,
					tournament_type : $scope.tournament_type,
					num_of_teams : $scope.numofteams,
					num_of_players : $scope.numofplayers,
					num_of_game_against : $scope.numofgameagainst,
					image_url: $scope.imageUrl,
					description: $scope.description,
					dateStart: $scope.currentDate,
					dateFinish: $scope.finalDate,
					author : 'user'
				});
				//clear the values
				$scope.title = '';
				$scope.sport_type = '';
				$scope.tournament_type = '';
				$scope.numofteamsinknoc = '';

			}; 
   		}
   		else if($scope.tournament_type === 'Knockout'){
   			$scope.knockout = true;
   			$scope.league = false;
			$scope.addPost = function() {
					if ($scope.title === '') {
						return;
					}
					posts.create({
						title : $scope.title,
						sport_type : $scope.sport_type,
						tournament_type : $scope.tournament_type,
						num_of_teams : $scope.numofteams,
						num_of_players : $scope.numofplayers,
						num_of_teams_knockout : $scope.numofteamsinknoc,
						num_of_knockout_against : $scope.knockagainst,
						final_knockout_ag : $scope.knockagainstfinal,
						image_url: $scope.imageUrl,
						description: $scope.description,
						dateStart: $scope.currentDate,
						dateFinish: $scope.finalDate,
						author : 'user'
					});
					//clear the values
					$scope.title = '';
					$scope.numofteamsinknoc = '';
					$scope.sport_type = '';
					$scope.tournament_type = '';
					$scope.numofteamsinknoc = '';
				}; 
   		}
   		else if($scope.tournament_type === 'League and knockout'){
   			$scope.knockout = true;
   			$scope.league = true;
			$scope.addPost = function() {
					if ($scope.title === '') {
						return;
					}
					posts.create({
						title : $scope.title,
						sport_type : $scope.sport_type,
						tournament_type : $scope.tournament_type,
						num_of_teams : $scope.numofteams,
						num_of_players : $scope.numofplayers,
						num_of_game_against : $scope.numofgameagainst,
						num_of_teams_knockout : $scope.numofteamsinknoc,
						num_of_knockout_against : $scope.knockagainst,
						final_knockout_ag : $scope.knockagainstfinal,
						image_url: $scope.imageUrl,
						description: $scope.description,
						dateStart: $scope.currentDate,
						dateFinish: $scope.finalDate,
						author : 'user'
					});
					//clear the values
					$scope.title = '';
					$scope.numofteamsinknoc = '';
					$scope.sport_type = '';
					$scope.tournament_type = '';
					$scope.numofteamsinknoc = '';
				};   		
		}
		else{
			$scope.knockout = false;
   			$scope.league = false;
		}
    });


	$scope.upvote = function(post) {
		//our post factory has an upvote() function in it
		//we're just calling this using the post we have
		console.log('Upvoting:' + post.title + "votes before:" + post.upvotes);
		posts.upvote(post);
	};
	$scope.downvote = function(post) {
		posts.downvote(post);
	};

	 this.showDatePicker = function(ev) {
      $mdpDatePicker($scope.currentDate, {
        targetEvent: ev
      }).then(function(selectedDate) {
        $scope.currentDate = selectedDate;
      });;
    };

    	$scope.minDate = new Date();
	 
	    var maxDay = new Date();
	    maxDay.setMonth(maxDay.getMonth()+12);
	    $scope.maxDay = new Date(maxDay.getFullYear(),maxDay.getMonth() , maxDay.getDate());



    this.filterDate = function(date) {
      return moment(date).date() % 2 == 0;
    };
   
    this.showTimePicker = function(ev) {
      $mdpTimePicker($scope.currentTime, {
        targetEvent: ev
      }).then(function(selectedDate) {
        $scope.currentTime = selectedDate;
      });;
    }

}]);