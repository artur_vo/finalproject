var mongoose = require('mongoose');

var joinUserSchema = new mongoose.Schema({
  	author: {type: String} ,
  	teamname: {type: String},
    post: { type: mongoose.Schema.Types.ObjectId, ref: 'Post' }
});

mongoose.model('joinUser', joinUserSchema);