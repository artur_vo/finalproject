var mongoose = require('mongoose');

var matchSchema = mongoose.Schema({
	id: {type: Number, unique: true},
	team: String,
	score: Number,
	round: Number,
	wins: {type: Number, default: 0},
	draws: {type: Number, default: 0},
	losses: {type: Number, default: 0},
	against: {type: Number, default: 0},
	difference: {type: Number, default: 0},
	points: {type: Number, default: 0},
	post: { type: mongoose.Schema.Types.ObjectId, ref: 'Post' }

});

// matchSchema.methods.dudify = function() {
//   // add some stuff to the users name
//   this.team = this.team + '-dude'; 

//   return this.team;
// };

mongoose.model('Match', matchSchema);