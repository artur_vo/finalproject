var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
  title: {type: String, trim: true},
  author: String,
  sport_type: String,
  tournament_type: String,
  num_of_teams: {type: Number, default: 0},
  num_of_players: {type: Number, default: 0},
  num_of_game_against: {type: Number, default: 0},
  num_of_teams_knockout: String,
  num_of_knockout_against: {type: Number, default: 0},
  final_knockout_ag: {type: Number, default: 0},
  image_url: {type: String, trim: true},
  description: String,
  dateStart: {type: Date},
  dateFinish: {type: Date},
  createdDate: {type: Date, default: Date.now},
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
  joinusers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'joinUser' }],
  allmatches: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Match' }]
});

// the schema is useless so far we need to create a model using it
mongoose.model('Post', PostSchema);