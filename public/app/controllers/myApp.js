angular.module('myApp.controllers', [
    "ngMaterial",
    "ngAnimate",
    "ngMessages",
    "ngAria",
    "mdPickers",
    'ui.router',
    'ui.bootstrap'
  ]);