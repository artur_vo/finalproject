describe('Create post', function () {

beforeEach(module('myApp.factory'));

  var $httpBackend,posts;

  beforeEach(inject(function(_$httpBackend_, _posts_) {
  	posts = _posts_;
    $httpBackend = _$httpBackend_;


  }));
  describe('Post factory', function () {
	  it('create post on the server', function() {

	    $httpBackend.expectPOST('/posts', {
	      "title":"Hello",
	      "sport_type":"Soccer",
	      "tournament_type":"League",
	      "num_of_teams":6,
	      "num_of_players":1,
	      "num_of_game_against":1,
	      "image_url":"",
	      "description":""
	    })
	    .respond(200);
	    var succeeded;
	    posts.create({
	      "title":"Hello",
	      "sport_type":"Soccer",
	      "tournament_type":"League",
	      "num_of_teams":6,
	      "num_of_players":1,
	      "num_of_game_against":1,
	      "image_url":"",
	      "description":""
	    }).then(function () {
	      succeeded = true;
	    });
	    $httpBackend.flush();
	    expect(succeeded).toBe.true;

	  });
	  it('get all post from the server', function() {

	    $httpBackend.expectGET('/posts')
	    .respond(200);
	    var succeeded;

	    posts.getAll().then(function () {
	      succeeded = true;
	    });

	    $httpBackend.flush();
	    expect(succeeded).toBe.true;

	  });
	  it('it should get single post', function() {
	  	
	    $httpBackend.expectGET('/posts/12154')
	    .respond(200);

	    posts.get("12154").then(function () {
	      succeeded = true;
	    });

	    $httpBackend.flush();
	    $httpBackend.verifyNoOutstandingExpectation();
   		$httpBackend.verifyNoOutstandingRequest();
   		expect(succeeded).toBe.true;
	  });
	  it('it add comment to single post', function() {
	  	
	    $httpBackend.expectPOST('/posts/1215/comments',{
	    	"body": "Hello",
  			"author": "user",
	    }
	    	
	    ).respond(200);

	    posts.addComment("1215", {
	    	"body": "Hello",
  			"author": "user",
	    }).then(function () {
	      succeeded = true;
	    });

	    $httpBackend.flush();
	    expect($httpBackend.verifyNoOutstandingExpectation()).toBe.true;
   		expect($httpBackend.verifyNoOutstandingRequest()).toBe.true;
   		expect(succeeded).toBe.true;
	  });
	  it('it add users to single post', function() {
	  	
	    $httpBackend.expectPOST('/posts/1215/joinusers',{
  			"author": "user",
	    }).respond(200);

	    posts.addUser("1215", {
  			"author": "user",
	    }).then(function () {
	      succeeded = true;
	    });

	    $httpBackend.flush();
	    expect($httpBackend.verifyNoOutstandingExpectation()).toBe.true;
   		expect($httpBackend.verifyNoOutstandingRequest()).toBe.true;
   		expect(succeeded).toBe.true;
	  });
	  it('it add users to single post', function() {
	  	
	    $httpBackend.expectPOST('/posts/1215/allmatches/',{
  			"id": "01",
  			"team": "A"
	    }).respond(200);

  			
	    posts.tableScore("1215", {
  			"id": "01",
  			"team": "A"
	    }).then(function () {
	      succeeded = true;
	    });
	    
	    $httpBackend.flush();
	    expect($httpBackend.verifyNoOutstandingExpectation()).toBe.true;
   		expect($httpBackend.verifyNoOutstandingRequest()).toBe.true;
   		expect(succeeded).toBe.true;
	  });
	});
});