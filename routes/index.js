var mongoose = require('mongoose');
var express = require('express');
var router = express.Router();
var passport = require('passport');
var jwt = require('express-jwt');
var sendgrid  = require('sendgrid')('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');


var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');
var Match = mongoose.model('Match');
var User = mongoose.model('User');
var joinUser = mongoose.model('joinUser');

var auth = jwt({secret: 'SECRET', userProperty: 'payload'});



/* GET home page. */
router.get('/', function(req, res) {
  res.render('index');
});

router.get('/posts', function(req, res, next) {
  Post.find(function(err, posts){
    if(err){ 
      return next(err);
    }

    res.json(posts);
  });
});

router.post('/posts', auth, function(req, res, next) {
  var post = new Post(req.body);
  post.author = req.payload.username;

  post.save(function(err, post){
    if(err){ return next(err); }

    res.json(post);
  });
});

router.param('post', function(req, res, next, id) {
  var query = Post.findById(id);

  query.exec(function (err, post){
    if (err) { return next(err); }
    if (!post) { return next(new Error("can't find post")); }

    req.post = post;
    return next();
  });
});

router.param('comment', function(req, res, next, id) {
  var query = Comment.findById(id);

  query.exec(function (err, comment){
    if (err) { return next(err); }
    if (!comment) { return next(new Error("can't find comment")); }

    req.comment = comment;
    return next();
  });
});

router.param('joinuser', function(req, res, next, id) {
  var query = joinUser.findById(id);

  query.exec(function (err, joinuser){
    if (err) { return next(err); }
    if (!joinuser) { return next(new Error("can't find joined user")); }
    req.joinuser = joinuser;
    return next();
  });
});

router.param('match', function(req, res, next, id) {
  var query = Match.findById(id);

    Match.find({}).sort({id: -1}).exec(function(err, results){
    if(err){return next(err);}
  });
    
  query.exec(function (err, match){
    if (err) { return next(err); }
    if (!match) { return next(new Error("can't find match")); }

    req.match = match;
    return next();
  });
});

router.get('/posts/:post', function(req, res, next) {
  req.post.populate('joinusers comments allmatches', function(err, post) {
    res.json(post);
  });
});


router.post('/posts/:post/comments', auth, function(req, res, next) {
  var comment = new Comment(req.body);
  comment.post = req.post;
  comment.author = req.payload.username;
  
  comment.save(function(err, comment){
    if(err){ return next(err); }

    req.post.comments.push(comment);
    req.post.save(function(err, post) {
      if(err){ return next(err); }

      res.json(comment);
    });
  });
});

router.post('/posts/:post/joinusers', auth, function(req, res, next) {
  var joinuser = new joinUser(req.body);
  joinuser.post = req.post;
  joinuser.author = req.payload.username;
  
  joinuser.save(function(err, joinuser){
    if(err){ return next(err); }

    req.post.joinusers.push(joinuser);
    req.post.save(function(err, post) {
      if(err){ return next(err); }

      res.json(joinuser);
    });
  });
});


router.post('/posts/:post/allmatches', auth, function(req, res, next) {
  var match = new Match(req.body);
  match.post = req.post;

  match.save(function(err, match){
    if(err){ return next(err); }


    req.post.allmatches.push(match);
    req.post.save(function(err, post) {
      if(err){ return next(err); }

      res.json(match);
    });
  });
});


// router.put('/posts/:post/allmatches/:match/update', auth, function(req, res, next) {
//     Match.update(function(err,match) {
//       if(err){ return next(err + "   error"); }

//       res.json(match);
//     });
// });

router.put('/posts/:post/comments/:comment/upvote', auth, function(req, res, next) {
  req.comment.upvote(function(err, comment){
    if (err) { return next(err); }

    res.json(comment);
  });
});

router.put('/posts/:post/comments/:comment/downvote', auth, function(req, res, next) {
  req.comment.downvote(function(err, comment){
    if (err) { return next(err); }

    res.json(comment);
  });
});

router.post('/register', function(req, res, next){
  if(!req.body.username || !req.body.password){
    return res.status(400).json({message: 'Please fill out all fields'});
  }

  var user = new User();

  user.username = req.body.username;

  user.email = req.body.email;

  user.setPassword(req.body.password)

  user.save(function (err){
    if(err){ return next(err); }

    return res.json({token: user.generateJWT()})
  });
});

router.post('/login', function(req, res, next){
  if(!req.body.username || !req.body.password){
    return res.status(400).json({message: 'Please fill out all fields'});
  }

console.log('calling passport)');
  passport.authenticate('local', function(err, user, info){
    if(err){ return next(err); }

    if(user){
      return res.json({token: user.generateJWT()});
    } else {
      return res.status(401).json(info);
    }
  })(req, res, next);
});


router.post('/#/contact', function(req,res,next){

    sendgrid.send({
      to:       'arturv1404@gmail.com',
      from:     'otheremail',
      subject:  'First Message',
      text:     'My first email through SendGrid hahahah.'
    }, function(err, json) {
      if (err) { return console.error(err); }
      console.log(json);
    });
});


module.exports = router;