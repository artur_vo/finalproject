describe('Authorization', function () {

beforeEach(module('myApp.factory'));

  var $httpBackend,auth;

  beforeEach(inject(function(_$httpBackend_, _auth_) {
  	auth = _auth_;
    $httpBackend = _$httpBackend_;

  }));

  describe('Auth factory', function () {
  	it('should have Auth service be defined', function () {
	   expect(auth).toBeDefined();
	});
  	it('should not have a user existing upon starting up', function() {
	   expect(auth.currentUser()).toBe('artur');
	});
	
	it('should return false if a user is not logged in', function () {
      expect(auth.isLoggedIn()).toEqual(false);
    });
	it('should return true if a user is logged in', function () {
	  spyOn(Date, 'now').and.returnValue(2000);

	  user = {
	  	username: 'artur',
	    email: 'kama3uk@hotmail.com',
	    password: '12345678'
	  };

	  $httpBackend.expectPOST('/login')
	  .respond({ token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NzRjYTE3MWRkNTIxMmEwMTc1NzY1NDEiLCJ1c2VybmFtZSI6ImFydHVyIiwiZXhwIjoxNDY1NTA3NDc3LCJpYXQiOjE0NjQ2NDM0Nzd9.ImEfTnqDEsYpCg5ps02lw37Wt9EYPyxJe_GmLhFiHwU' });
	  auth.logIn(user);
	  $httpBackend.flush();
	  expect(auth.isLoggedIn()).toEqual(true);


	});
	it('should return false if a users token has expired', function () {
	   jasmine.clock().install();
  	   jasmine.clock().mockDate(new Date(2020, 11, 30));

	  user = {
	  	username: 'artur',
	    email: 'kama3uk@hotmail.com',
	    password: '12345678'
	  };

	  $httpBackend.expectPOST('/login')
	  .respond({ token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NzRjYTE3MWRkNTIxMmEwMTc1NzY1NDEiLCJ1c2VybmFtZSI6ImFydHVyIiwiZXhwIjoxNDY1NTA3NDc3LCJpYXQiOjE0NjQ2NDM0Nzd9.ImEfTnqDEsYpCg5ps02lw37Wt9EYPyxJe_GmLhFiHwU' });
	  auth.logIn(user);
	  $httpBackend.flush();
	  expect(auth.isLoggedIn()).toEqual(false);

	  jasmine.clock().uninstall();
	});

	});
	it('should registered and login', function () {
	  spyOn(Date, 'now').and.returnValue(2000);

	  user = {
	  	username: 'artur',
	    email: 'kama3uk@hotmail.com',
	    password: '12345678'
	  };

	  $httpBackend.expectPOST('/register')
	  .respond({ token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1NzRjYTE3MWRkNTIxMmEwMTc1NzY1NDEiLCJ1c2VybmFtZSI6ImFydHVyIiwiZXhwIjoxNDY1NTA3NDc3LCJpYXQiOjE0NjQ2NDM0Nzd9.ImEfTnqDEsYpCg5ps02lw37Wt9EYPyxJe_GmLhFiHwU' });
	  auth.register(user);
	  var currUser = auth.currentUser(); 
	  expect(currUser).toBe(user.username);

	});
});